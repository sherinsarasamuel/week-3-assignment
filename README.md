# Week 3 Assignment

Create a Class Pet. Each instance of the class will be one electronic pet for the user to take care of. Each instance will have a current state, consisting of three Instance variables:

Hunger - integer

Boredom -  an integer

Sounds - a list of strings, each a word that the pet has been taught to say

Methods

In the __init__ method of the class, hunger and boredom are initialized to random values between 0 and the threshold for being hungry or bored. Initialize the sounds instance variable appropriately so that teaching a sound to any of the pets would not teach it to all instances of the class!

There is a clock_tick method which just increments the boredom and hunger instance variables, simulating the idea that as time passes, the pet gets more bored and hungry.

The __str__ method produces a string representation of the pet’s current state, notably whether it is bored or hungry or whether it is happy. It’s bored if the boredom instance variable is larger than the threshold, which is set as a class variable.

To relieve boredom, the pet owner can either teach the pet a new word, using the teach() method, or interact with the pet, using the hi() method. In response to teach(), the pet adds the new word to its list of words. In response to the hi() method, it prints out one of the words it knows, randomly picking one from its list of known words. Both hi() and teach() cause an invocation of the reduce_boredom() method. It decrements the boredom state by an amount that it reads from the class variable boredom_decrement. The boredom state can never go below 0.

To relieve hunger, we call the feed() method which will cause an invocation of the reduce_hunger() method.

Features

- Interact with the user. At each iteration, display a text prompt reminding the user of what commands are available.

- The user will have a list of pets, each with a name. The user can issue a command to adopt a new pet, which will create a new instance of Pet. Or the user can interact with an existing pet, with a Greet, Teach, or Feed command.

- Teach new words to pets so that each pet knows a different set of words.

- No matter what the user does, with each command entered, the clock ticks for all their pets. 
