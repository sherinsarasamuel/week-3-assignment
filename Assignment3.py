# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 10:58:25 2020

@author: user
"""
from random import shuffle
threshold=7
decrement=2

class Pet():



    def __init__(self,name,hunger=0,boredom=0,sounds=[]):
        self.hunger=hunger
        self.boredom=boredom
        self.sounds=sounds
        self.name=name;
        
    def __str__(self):
        print(self.name,'Report :')
        print( 'Hunger level is ',self.hunger)
        print('Boredom is ',self.boredom)
        if self.hunger>threshold or self.boredom>threshold :
            return 'Pet is sad.'
        else:
            return 'Pet is happy'
        
    def clock_tick(self):
        self.boredom+=1;
        self.hunger+=1;
        
    def teach(self):
        word=input('What word would you like to teach? ')
        self.sounds.append(word)
        print('List of words ',self.name,'knows ',self.sounds)
        self.reduce_boredom()
        
    def hi(self):
        shuffle(self.sounds)
        print(self.name,'says' , self.sounds[0])
        self.reduce_boredom()

    def reduce_boredom(self):
        self.boredom = max(0, self.boredom - decrement)
        print('Boredom level is now', self.boredom)
        
    def feed(self):
        print(self.name, 'is fed ')
        self.reduce_hunger()
        
    def reduce_hunger(self):
        self.hunger = max(0, self.hunger - decrement)
        print('Hunger level is now ',self.hunger)
        
Blackie=Pet("Blackie",4,6,["sit","stand","roll"])
Jimmy=Pet("Jimmy",3,7,["woof","sit","play"])
Rocky=Pet("Rocky",2,5,["Nod","bark"])
stat='y'
while stat=='Y' or stat=='y':
    
    Blackie.clock_tick()
    Jimmy.clock_tick()
    Rocky.clock_tick()
    pet=input("Choose which pet [Blackie, Rocky, Jimmy] ")
    action=input("Choose action [Greet, Feed, Teach] ")
    if(pet=='Blackie'):
        if(action=='Greet'):
            Blackie.hi()
        elif action=='Feed':
            Blackie.feed()
        elif action=='Teach':
            Blackie.teach()
        else:
            print('Enter valid action')
        #print(Blackie)
    elif(pet=='Rocky'):
        if(action=='Greet'):
            Rocky.hi()
        elif action=='Feed':
            Rocky.feed()
        elif action=='Teach':
            Rocky.teach()
        else:
            print('Enter valid action')
       # print(Rocky)
    elif(pet=='Jimmy'):
        if(action=='Greet'):
            Jimmy.hi()
        elif action=='Feed':
            Jimmy.feed()
        elif action=='Teach':
            Jimmy.teach()
        else:
            print('Enter valid action')
       # print(Jimmy)
    else:
        print('Enter valid name. Check Spelling')
        continue
    print(Blackie)
    print()
    print(Rocky)
    print()
    print(Jimmy)
    stat=input('Do you want to continue Y/y ')
